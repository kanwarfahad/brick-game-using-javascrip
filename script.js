var canva = document.getElementById("mycanvas");
var ctx = canva.getContext("2d");


var x = canva.width/2;
var y = canva.height -30;
var dx = 2;
var dy = -2;
var ballradius = 10;
var paddleheight = 10;
var paddlewidth = 95;
var paddlex = (canva.width - paddlewidth)/2;
var rightPressed = false;
var leftPressed = false;
var brickrowcount = 6;
var brickcolcount = 10;
var brickwidth = 75;
var brickheight = 20;
var brickpadding = 10;
var brickoffsettop = 50;
var brickoffsetleft = 64;
var score = 0;
var lives = 3;


var brick = [];
for (c=0; c<brickcolcount; c++){
	brick[c] = [];
	for(r=0; r<brickrowcount; r++){
		brick[c][r] = {x:0 ,y:0, status:1};
	}
}

document.addEventListener("keydown" , keyDownHandler);
document.addEventListener("keyup" , keyUpHandler);



function keyDownHandler(e){
	if(e.keyCode == 39){
		rightPressed = true;
	}
	else if(e.keyCode == 37){
		leftPressed = true;
	}
}
function keyUpHandler(e){
	if(e.keyCode == 39){
		rightPressed = false;
	}
	else if(e.keyCode == 37){
		leftPressed = false;
	}
}

function drawbrick(){
	for (c=0; c<brickcolcount; c++){
	for(r=0; r<brickrowcount; r++){
		if(brick[c][r].status == 1){
		var brickX = (c*(brickwidth+brickpadding)) + brickoffsetleft;
		var brickY = (r*(brickheight+brickpadding)) + brickoffsettop;
		
		brick[c][r].x = brickX;
		brick[c][r].y = brickY;
		
		ctx.beginPath();
		ctx.rect(brickX,brickY, brickwidth , brickheight)
		ctx.fillstyle = "#0095DD";
		ctx.fill();
		ctx.closePath();
		}
	}
}
}

function brickcollision(){
	for (c=0; c < brickcolcount; c++){
	for(r=0; r < brickrowcount; r++){
		var b =brick[c][r];
		if(b.status == 1){
		if(x > b.x && x < b.x + brickwidth && y > b.y && y < b.y+brickheight){
			dy = -dy;
			b.status = 0;
			score++;
			if(score == brickrowcount*brickcolcount){
				alert("You Win The Game!!!!");
				document.location.reload();
						}	
					}
				}
			}
		}
	}

	
function drawball(){
	ctx.beginPath();
	ctx.arc(x , y, ballradius, 0, Math.PI*2);
	ctx.fillStyle = "#0095DD";
	ctx.fill();
	ctx.closePath();
}

function drawpaddle(){
	
	ctx.beginPath();
	ctx.rect(paddlex, canva.height - paddleheight , paddlewidth , paddleheight);
	ctx.fillStyle = "#0095DD";
	ctx.fill();
	ctx.closePath();
}

function drawscore(){
	ctx.font = "20px Arial";
	ctx.fillStyle = "#0095DD";
	ctx.fillText("Score: "+score, 8, 20);
}	

function drawlive(){
	ctx.font = "20px Arial";
	ctx.fillStyle = "#0095DD";
	ctx.fillText("Lives: "+lives , canva.width-75 , 20);
	}

function draw(){
	
	ctx.clearRect(0, 0, canva.width, canva.height);
	drawball();
	drawpaddle();
	drawbrick();
	drawscore();
	drawlive();
	brickcollision();
	
	if(y + dy < ballradius){
		dy = -dy; 
	}
	else if(y + dy > canva.height-ballradius){
		
		if(x > paddlex && x < paddlex + paddlewidth){
			dy = -dy; 
		}
		else{
			lives --;
			if(!lives){
			alert("Your Game is over");
			document.location.reload();
			}
			else{
				x=canva.width/2;
				y = canva.height-30;
				dx = 2;
				dy = -2;
				paddlex = (canva.width - paddlewidth)/2;
				
			}
		}
	}
		
	if(x + dx > canva.width-ballradius || x + dx < ballradius){
		dx = -dx; 
	}
	
	if(rightPressed && paddlex < canva.width - paddlewidth){
		paddlex +=7;
		}
	else if (leftPressed && paddlex > 0){
		paddlex -=7;
	}
	
	x += dx;
	y += dy;
}
setInterval(draw , 10)
